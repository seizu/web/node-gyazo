FROM node:10-alpine as builder

WORKDIR /tmp/gyazo

# install lscpu
RUN set -x && \
    apk add --no-cache util-linux

COPY package.json /tmp/gyazo/
COPY src /tmp/gyazo/src

RUN set -x && \
    npm install && \
    npm run build

FROM node:10-alpine

LABEL maintainer "Takashi Endo <surface0@rainorshine.asia>"

ENV GYAZO_VIEW_HOST=http://127.0.0.1:3000 \
    GYAZO_VIEW_PATH=/view \
    GYAZO_UPLOAD_PATH=/upload \
    GYAZO_UPLOAD_FILE_PATH=/opt/gyazo/uploads

WORKDIR /opt/gyazo

RUN set -x && \
    mkdir -p uploads && \
    chmod a+w uploads

COPY --from=builder /tmp/gyazo/dist/app.js .

VOLUME /opt/gyazo/uploads

EXPOSE 3000

ENTRYPOINT ["node", "/opt/gyazo/app.js"]